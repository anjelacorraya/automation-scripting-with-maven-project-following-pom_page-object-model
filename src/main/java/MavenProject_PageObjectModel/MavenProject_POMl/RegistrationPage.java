package MavenProject_PageObjectModel.MavenProject_POMl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {

	
	WebDriver driver;
	public RegistrationPage(WebDriver driver) {
		
		this.driver = driver;
		//This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
	}
	
	
	//All WebElements are identified by  POM Annotation
	
//		@FindBy(xpath = "//INPUT[@id='input-firstname']")
//		WebElement FirstName;
		
		By FirstName_POM = By.xpath("//INPUT[@id='input-firstname']");
		
		By LastName_POM = By.xpath ("//INPUT[@id='input-lastname']");
		
		By Email_POM =By.xpath("//INPUT[@id='input-email']");
		
		By Telephone_POM = By.xpath("//INPUT[@id='input-telephone']");
		
		By Password_POM = By.xpath("//INPUT[@id='input-password']");
		
		By Confirmpassword_POM = By.xpath("//INPUT[@id='input-confirm']");
		
		By PrivacyPolicy_POM = By.xpath("//INPUT[@type='checkbox']");
		
		By SubmitRegistrationbutton_PPM = By.cssSelector("#content > form > div > div > input.btn.btn-primary");
		
		
		


		public void ProvideFirstName(String firstname ) {
			
			driver.findElement(FirstName_POM).sendKeys(firstname);
			//FirstName.sendKeys(firstname);
		}
		
		public void ProvideLastName(String lastname) {
			driver.findElement(LastName_POM).sendKeys(lastname);

		}


		public void ProvideEmailAddress(String email) {
			System.out.println("Given email is: "+ email);
			driver.findElement(Email_POM).sendKeys(email);
		
			
			
		}


		public void ProvidePhoneNumber(String telephone) {
			driver.findElement(Telephone_POM).sendKeys(telephone);
		
			
		}


		public void ProvidePassword(String password) {
			System.out.println("Given Password is: "+ password);
			driver.findElement(Password_POM).sendKeys(password);
			
			
		}


		public void ProvideConfirmPassword(String confirmpassword) {
			driver.findElement(Confirmpassword_POM).sendKeys(confirmpassword);
			
			
		}

		public void ClickOnPrivacyPolicy() {
			driver.findElement(PrivacyPolicy_POM).click();
			
			
		}
		
		public void ClickOnSubmitbutton() {
			
			driver.findElement(SubmitRegistrationbutton_PPM).click();
			
		}
		
		
}
